from flask import Flask, render_template, request
from forms import SignUpForm
import os

app = Flask(__name__)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = 'secrfgdgret'

posts = [
    {"author": "Barbara Go",
     "title": "Jumpin' Fun!",
     "content": "Lorem Ipsum dolor",
     "date_posted": "April 3, 2021"},
    {"author": "Dan Brown",
     "title": "Inferno",
     "content": "Chillax dude",
     "date_posted": "April 4, 2021"}
]

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/animal')
def animal():
    return render_template('animal.html', posts=posts, title="Fun Animal Facts")

@app.route('/signup', methods=["GET", "POST"])
def signup():
    form = SignUpForm()
    if form.is_submitted():
        result = request.form
        return render_template("userdata.html", result=result)
    return render_template('signup.html',form=form)